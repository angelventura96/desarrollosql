SELECT commission_pct AS "commission", salary,
NVL2(commission_pct, salary + (salary * commission_pct), salary)
AS salarioXcomision
FROM employees
WHERE department_id IN(80,90);


/*
firstname
longitud firstname
lastname
longitud lastname
si ambas longitudes son oguales regresa null, si no lo son regresa la longitud FN*/
SELECT 
first_name, 
LENGTH(first_name) AS "Length FN", 
last_name,
LENGTH(last_name) AS "Length LN", 
NULLIF(LENGTH(first_name), LENGTH(last_name)) AS "Compare Them"
FROM employees;

/*COALESCE evalua si hay muchos condicones null hasta encontrar una condicion
que no lo sea
KING (36), su comision es null, su manager es null, por eso muestra el valor 777*/
SELECT last_name,
COALESCE(commission_pct, manager_id, 777)
AS "Comm"
FROM employees
ORDER BY commission_pct;

