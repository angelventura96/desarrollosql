/*SelfJoin
Una tabla se relaciona consigo misma atraves de diferentes alias para la misma tabla
un empleado tiene un gerente, pero el gerente es un empleado, que a su vez tiene
un gerente*/
SELECT worker.last_name || ' works for ' || manager.last_name
AS "Works for"
FROM employees worker JOIN employees manager
ON (worker.manager_id = manager.employee_id);

SELECT LPAD(last_name, LENGTH(last_name)+(LEVEL*2)-2,'_')
AS "Org Chart"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR employee_id = manager_id;

SELECT LPAD(last_name, LENGTH(last_name)+
(LEVEL*2)-2,'_') AS "Org_Chart"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR employee_id = manager_id;
