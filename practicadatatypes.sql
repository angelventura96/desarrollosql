/*Limpiar nuestro espacio de trabajo*/
DROP TABLE EMPLOYEES
DROP TABLE PERSONS
DROP TABLE projects
DROP TABLE participants
DROP TYPE PHONE_ARRAY FORCE
DROP TYPE PHONE_TAB FORCE
DROP TYPE PERSON FORCE
DROP TYPE ADDRESS FORCE
DROP TYPE moduletbl_t FORCE
DROP TYPE module_t FORCE
DROP TYPE participant_t FORCE

/*Crear un objeto TIPO ADDRESS */
CREATE TYPE address AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
)

/*Crear un objeto tipo PErSON que tiene un campo de tipo address que se creo anteriormente*/
CREATE TYPE person AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    address
)

/*Crear tabla de tipo PERSON*/
CREATE TABLE persons OF person

/*Crear un objeto tipo arrar PHONE_ARRAY, se podran guardar hasta 10 telefonos de 30 caract max*/
CREATE TYPE PHONE_ARRAY IS VARRAY (10) OF varchar2(30)

/*Crear objeto de tipo participante*/
CREATE TYPE participant_t AS OBJECT (
  empno   NUMBER(4),
  ename   VARCHAR2(20),
  job     VARCHAR2(12),
  mgr     NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  deptno   NUMBER(2)) 
  
/*Crear objeto tipo module_t*/
CREATE TYPE module_t  AS OBJECT (
  module_id  NUMBER(4),
  module_name VARCHAR2(20), 
  module_owner REF participant_t , 
  module_start_date DATE, 
  module_duration NUMBER )
  
/*Crear un tipo moduletbl_t de tipo tabla de tipo module_t*/
create TYPE moduletbl_t AS TABLE OF module_t;

/*Crear la tabla employees, usa campos de tipo de dato definidos anteiormente*/
CREATE TABLE  employees
( empnumber            INTEGER PRIMARY KEY,
  person_data     REF  person,
  manager         REF  person,
  office_addr          address,
  salary               NUMBER,
  phone_nums           phone_array
)

CREATE TABLE projects (
  id NUMBER(4),
  name VARCHAR(30),
  owner REF participant_t,
  start_date DATE,
  duration NUMBER(3),
  modules  moduletbl_t  ) NESTED TABLE modules STORE AS modules_tab ;

/*tabla con base en un objeto de tipo participant_t*/
CREATE TABLE participants  OF participant_t ;

INSERT INTO persons VALUES (
            person('Wolfgang Amadeus Mozart', 123456,
            address('Am Berg 100', 'Salzburg', 'AU','10424')))

INSERT INTO persons VALUES (
            person('Ludwig van Beethoven', 234567,
            address('Rheinallee', 'Bonn', 'DE', '69234')))

/*
INSERT INTO employees (empnumber, person_data, manager, office_addr, salary) VALUES (
            101,
            (SELECT REF(p) FROM persons p WHERE p.name = 'Marco Antonio'),
            (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven'),
            ADDRESS('San Juan', 'Zumpango', 'MX', '59847'),
            50000);
*/

INSERT INTO employees (empnumber, office_addr, salary, phone_nums) VALUES 
            (1001,
             address('500 Oracle Parkway', 'Redwood City', 'CA', '94065'),
             50000,
             phone_array('(408) 555-1212', '(650) 555-9999'));
             
UPDATE employees 
  SET manager =  
    (SELECT REF(p) FROM persons p WHERE p.name = 'Wolfgang Amadeus Mozart')

UPDATE employees 
  SET person_data =  
    (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven')

/*------------------------------------------------------------------------------------------*/
INSERT INTO participants VALUES (
participant_T(7369,'ALAN SMITH','ANALYST',7902,to_date('17-12-1980','dd-mm-yyyy'),800,20)) ;

INSERT INTO participants VALUES (
participant_t(7499,'ALLEN TOWNSEND','ANALYST',7698,to_date('20-2-1981','dd-mm-yyyy'),1600,30));

INSERT INTO participants VALUES (
participant_t(7521,'DAVID WARD','MANAGER',7698,to_date('22-2-1981','dd-mm-yyyy'),1250,30));

INSERT INTO participants VALUES (
participant_t(7566,'MATHEW JONES','MANAGER',7839,to_date('2-4-1981','dd-mm-yyyy'),2975,20));

INSERT INTO participants VALUES (
participant_t(7654,'JOE MARTIN','MANAGER',7698,to_date('28-9-1981','dd-mm-yyyy'),1250,30));

INSERT INTO participants VALUES (
participant_t(7698,'PAUL JONES','Director',7839,to_date('1-5-1981','dd-mm-yyyy'),2850,30));

INSERT INTO participants VALUES (
participant_t(7782,'WILLIAM CLARK','MANAGER',7839,to_date('9-6-1981','dd-mm-yyyy'),2450,10));

INSERT INTO participants VALUES (
participant_t(7788,'SCOTT MANDELSON','ANALYST',7566,to_date('13-JUL-87','dd-mm-yy')-85,3000,20));

INSERT INTO participants VALUES (
participant_t(7839,'TOM KING','PRESIDENT',NULL,to_date('17-11-1981','dd-mm-yyyy'),5000,10));

INSERT INTO participants VALUES (
participant_t(7844,'MARY TURNER','SR MANAGER',7698,to_date('8-9-1981','dd-mm-yyyy'),1500,30));

INSERT INTO participants VALUES (
participant_t(7876,'JULIE ADAMS','SR ANALYST',7788,to_date('13-JUL-87', 'dd-mm-yy')-51,1100,20));

INSERT INTO participants VALUES (
participant_t(7900,'PAMELA JAMES','SR ANALYST',7698,to_date('3-12-1981','dd-mm-yyyy'),950,30));

INSERT INTO participants VALUES (
participant_t(7902,'ANDY FORD','ANALYST',7566,to_date('3-12-1981','dd-mm-yyyy'),3000,20));

INSERT INTO participants VALUES (
participant_t(7934,'CHRIS MILLER','SR ANALYST',7782,to_date('23-1-1982','dd-mm-yyyy'),1300,10));


INSERT INTO projects VALUES ( 101, 'Emarald', null, '10-01-98',  300, 
     moduletbl_t( module_t ( 1011 , 'Market Analysis', null, '01-01-98', 100),
                module_t ( 1012 , 'Forecast', null, '05-02-98',20) ,
                module_t ( 1013 , 'Advertisement', null, '15-03-98', 50),
                module_t ( 1014 , 'Preview', null, '15-03-98',44),
                module_t ( 1015 , 'Release', null,'12-05-98',34) ) ) ;
                
update projects set owner=(select ref(p) from participants p where p.empno = 
7839) where id=101 ;

update the ( select modules from projects a where a.id = 101 )  
set  module_owner = ( select ref(p) from participants p where p.empno = 7844) 
where module_id = 1011 ;

update the ( select modules from projects where id = 101 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7934) 
where module_id = 1012 ;

update the ( select modules from projects where id = 101 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7902) 
where module_id = 1013 ;

update the ( select modules from projects where id = 101 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7876) 
where module_id = 1014 ;

update the ( select modules from projects where id = 101 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7788) 
where module_id = 1015 ;

/*-----------------------*/

INSERT INTO projects VALUES ( 500, 'Diamond', null, '15-02-98', 555, 
       moduletbl_t ( module_t ( 5001 , 'Manufacturing', null, '01-03-98', 120),
                   module_t ( 5002 , 'Production', null, '01-04-98',100),
                   module_t ( 5003 , 'Materials', null, '01-05-98',200) ,
                   module_t ( 5004 , 'Marketing', null, '01-06-98',10) ,
                   module_t ( 5005 , 'Materials', null, '15-FEB-99',50),
                   module_t ( 5006 , 'Finance ', null, '16-02-99',12),
                   module_t ( 5007 , 'Budgets', null, '10-03-99',45))) ;
            
            
update projects set owner=(select ref(p) from participants p where p.empno =        
7698) where id=500 ;

update the ( select modules from projects where id = 500 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7369) 
where module_id = 5001 ;

update the ( select modules from projects where id = 500 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7499) 
where module_id = 5002 ;

update the ( select modules from projects where id = 500 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7521) 
where module_id = 5004 ;

update the ( select modules from projects where id = 500 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7566) 
where module_id = 5005 ;

update the ( select modules from projects where id = 500 ) 
set  module_owner = ( select ref(p) from participants p where p.empno = 7654) 
where module_id = 5007 ;

SELECT p.name, p.ssn, p.addr.street, p.addr.city, p.addr.state, p.addr.zip_code
FROM persons p;

SELECT e.empnumber, e.person_data.name, e.person_data.ssn, e.person_data.addr.city,
    e.manager.name,e.manager.ssn, e.manager.addr.city, e.office_addr.city,
    e.salary,e.phone_nums
    FROM employees e;