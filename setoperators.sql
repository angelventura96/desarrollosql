/*to_date(null) crea la columna ficticia hire_date para la tabla job_history
ya que para poder hacer el union de las 3 columanas, tienen que empatar
las estructuras de las columnas*/
SELECT hire_date, employee_id, job_id
FROM employees
UNION
SELECT TO_DATE(NULL),employee_id, job_id
FROM job_history;

SELECT hire_date, employee_id, TO_DATE(null) start_date,
TO_DATE(null) end_date, job_id, department_id
FROM employees
UNION
SELECT TO_DATE(null), employee_id, start_date, end_date, job_id,
department_id
FROM job_history
ORDER BY employee_id;