CREATE TABLE my_friends
(first_name VARCHAR2(20),
last_name VARCHAR2(30),
email VARCHAR2(30),
phone_num VARCHAR2(12),
birth_date DATE);

SELECT table_name, status
FROM USER_TABLES;

SELECT table_name, status
FROM ALL_TABLES;
