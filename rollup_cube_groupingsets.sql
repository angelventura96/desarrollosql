/*Muestra la suma del salario de cada jobib que hay en un departamento,
cuando no hay mas jobid para un departamento, muestra la suma de los salarios
de ese departamento, y al final muestra la suma de todos los trabajos de todos
los departmentos*/
SELECT department_id, job_id, count(employee_id), SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY ROLLUP (department_id, job_id);

/*Muestra el total de todo
Muestra totales por puesto sin importar el departamento
muestra totales por departamento sin importar los jobid
muestra totales por jobid por departamento*/
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);

/*Hace subgrupos multiples*/
SELECT department_id, job_id, manager_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY GROUPING SETS
((job_id, manager_id), (department_id, job_id), (department_id, manager_id));
