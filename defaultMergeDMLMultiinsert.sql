CREATE TABLE my_employees (
hire_date DATE DEFAULT SYSDATE,
first_name VARCHAR2(15),
last_name VARCHAR2(15));

INSERT INTO my_employees
(hire_date, first_name, last_name)
VALUES
(DEFAULT, 'Angelina','Wright');

INSERT INTO my_employees
(first_name, last_name)
VALUES
('Angelina','Wright');

UPDATE my_employees
SET hire_date = DEFAULT
WHERE last_name = 'Wright';

MERGE INTO copy_emp c USING employees e
ON (c.employee_id = e.employee_id)
WHEN MATCHED THEN UPDATE
SET
c.last_name = e.last_name,
c.department_id = e.department_id
WHEN NOT MATCHED THEN INSERT
VALUES (e.employee_id, e.last_name, e.department_id);


INSERT ALL
    INTO my_employees
                    VALUES (hire_date, first_name, last_name)
    INTO copy_my_employees
                    VALUES (hire_date, first_name, last_name)
SELECT hire_date, first_name, last_name
FROM employees;