1. ¿Qué hace la siguiente consulta?

SELECT 	MAX(salary)"Salario Mayor", 
		MIN(salary)"Salario menor", 
		AVG(salary)"Salario Promedio",
		SUM(Salary) "TOTAL Salarios"
FROM employees
WHERE hire_date <= '01/01/04' AND department_id = 50;

/*
R= Se solicita el salario máximo con el alias "Salario mayor", 
el salario más bajo con el alias "Salario menor", 
el promedio de todos los salarios con el alias "Salario promedio", 
y la suma de todos los salarios con el alias "TOTAL salarios", 
todo esto, de la tabla employees, 
donde se cumpla la condición, que la fecha de contración sea 
menor o igual a 01/01/04 y que el department_id sea igual a 50.
*/

2.Realiza una consulta para conocer la comisión más alta y la comisión promedio 
de los empleados, pero solamente del departamento 80. 
Cada Columna debe ir con su respectivo alias. 

/*
R=
SELECT 	MAX(commission_pct)"Comisión más alta", 
		AVG(commission_pct)"Comisión Promedio"
FROM employees
WHERE department_id = 80;
*/

3) Crea una tabla Estudiantes, con un campo para el nombre, apellido paterno,
 apellido materno, edad, y fecha de nacimiento, el campo fecha deberá 
 de tener un valor default null, y el apellido paterno no deberá 
 permitir valores nulos. 

 /*
 R=
CREATE TABLE Estudiantes (
	id_estudiante NUMBER(10) NOT NULL, 
	nombre VARCHAR2(30), 
	apellido_paterno VARCHAR2(30) NOT NULL, 
	apellido_materno VARCHAR2(30), 
	edad NUMBER(2), 
	fecha_nacimiento DATE DEFAULT NULL)
 */

4) ¿Qué hace el siguiente script? 

CREATE TABLE emp_load ( 
	employee_number CHAR(5), 
	employee_dob CHAR(20), 
	employee_last_name CHAR(20), 
	employee_first_name CHAR(15), 
ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY def_dir1 
ACCESS PARAMETERS (RECORDS DELIMITED BY NEWLINE FIELDS (employee_number CHAR(2), 
														employee_dob CHAR(20), 
														employee_last_name CHAR(18), 
														employee_first_name CHAR(11),
														"mm/dd/yyyy")) 
LOCATION ('info.dat')); 

/*R= Le indica a Oracle que debe crear una tabla externa desde info.dat*/

5) ¿Cuál es la diferencia entre ambos registros? 
			09-JUL-2020 05.42.30.000000 PM 
			09-JUL-2020 05.42.30.567945 PM 

/*R= El primer registro fue insertado con SYSDATE y el segundo con SYSTIMESTAMP */

6) Escribe el código para agregar una columna comida_favorita a la tabla 
Estudiantes con valor default null.

/*
R=	ALTER TABLE estudiantes
	ADD (comida_favorita varchar2(30) DEFAULT NULL);
*/

7) ¿Qué hace la siguiente línea? 

ALTER TABLE nombre_tabla SET UNUSED nombre_columna; 

/*R= Es la sintaxis básica para marcar una columna como UNUSED para no 
tener que eliminarla, de una tabla especifica
*/

8) Muestra en una consulta el nombre, apellido, nombre de departamento 
de todos los empleados, si no tiene departamento asignado muestra "No asignado". 
/*
R= 	SELECT 	e.first_name,   AQUI SE SELECCIONA EL NOMBRE
			e.last_name, 	AQUI SE SELECCIONA EL APELLIDO
			nvl(d.department_name,'**NO ASIGNADO**') AQUI SI EL DEPTID ES NULL, LO REEMPLAZA CON NO ASIGNADO
	FROM employees e LEFT OUTER JOIN departments d MUESTRA LOS EMPLEADOS QUE NO TENFAN UN DEPTO ASIGNADO
	ON (e.department_id=d.department_id); DE LOS REGISTROS DE EMPLOYEES Y DEPARTMENT COINCIDAD EN EL DEPTOID
*/

9) Muestra nombre, apellido, empleo actual, puestos anteriores y fecha de 
fin de cada puesto para todos los empleados que fueron contratados entre 2003 y 2005. 
/*
R=	SELECT	emp.first_name "nombre", 
			emp.last_name "apellido", 
			emp.job_id "Empleo actual", 
			emp.hire_date "Fecha de contratacion", 
			jobs.job_id "Viejo puesto",
			jobs.end_date "Fecha termino trabajo anterior"
	FROM employees emp LEFT OUTER JOIN job_history jobs
	ON (emp.employee_id = jobs.employee_id) 
	WHERE EXTRACT (YEAR FROM emp.hire_date) BETWEEN 2003 AND 2005 
	ORDER BY emp.employee_id;
*/

10) MUESTRA EL ID DE TRABAJADOR, LAST NAME, ID DE MANAGER ORDENADOS POR ID 
DE MANAGER (selfjoin) 
/*
R= Esto sería una tabla recursiva, por que se llama asi misma

	SELECT 	worker.employee_id "ID del trabajador", 
			worker.last_name "Apellido del trabajador", 
			manager.employee_id "MANAGER ID", 
			manager.last_name "APELLIDO MANAGER" 
	FROM employees worker JOIN employees manager 
	ON (worker.manager_id=manager.employee_id) 
	ORDER BY manager.employee_id;
*/

11) MUESTRA ID DE TRABAJADOR, LAST NAME, ID DE MANAGER, LAST NAME 
DE MANAGER CON UN QUERY DE JERARQUIA (El primer eslabon es el employee_id=100) 
Y ORDENARLOS POR EL ID DEL MANAGER (join de jerarquia) 
/*
R=	SELECT 	employee_id "ID TRABAJADOR", 
			last_name "APELLIDO TRABAJADOR", 
			manager_id "ID MANAGER, 
			PRIOR last_name as "APELLIDO MANAGER" 
	FROM employees 
	START WITH employee_id=100
	CONNECT BY PRIOR employee_id=manager_id 
	ORDER BY PRIOR employee_id;
*/

12) Explica lo que realiza la siguiente sentencia: 
SELECT AVG (DISTINCT salary) 
FROM employees 
WHERE department_id = 30; 

/*
R= Muestra el promedio de los salarios que son diferentes entre si,
es decir no considera valores repetidos, de la tabla empleados,
donde el department_id sea igual a 30
*/

13) Escribe la sentencia donde se muestre cuántos sueldos 
diferentes se pagan a los empleados del departamento 50 de tabla empleados. 
/*
R= 	SELECT COUNT(DISTINC(salary))
	FROM EMPLOYEES
	WHERE department_id = 50;
*/

14) Escribe la sentencia para obtener el nombre, apellido, el puesto y el 
salario de los empleados que tienen un salario mínimo mayor a $5,000, 
y ordénalo por salario.
/*
R=	SELECT first_name "nombre",
		   last_name "apellido",
		   job_title "puesto",
		   salary "salary",
	FROM employees NATURAL JOIN jobs
	WHERE min_Salary>5000
	ORDER BY salary
*/

15) Describe lo que realiza la siguiente sentencia. 

SELECT 	department_name, 
		street_address, 
		country_name 
FROM departments 
JOIN locations USING (location_id) JOIN countries USING (country_id) 
ORDER BY department_name; 

/*
R= Muestra el nombre del departamento
   la direccion de la calle
   el pais donde esta esa direccion
   de la tabla departments, 
   la tabla department y locations tienen a location_id, por eso es el JOIN.
   y la tabla locations y countries tienen el campo country_id, por eso se
   usa el otro join
*/

16) Se requiere una tabla para realizar el registro de inscripción de 
participantes a un concurso, donde se requiere un numero de boleto que 
será único y se registraran el nombre y apellido, hasta un máximo de 
50 participantes. Crear la tabla, la secuencia y agregar al menos 2 
participantes. 

/*
R=
CREATE TABLE tabla_concurso 
(boleto_id NUMBER(2,0) CONSTRAINT boleto_primary_key PRIMARY KEY, 
nombre_participante VARCHAR2(30), 
apellido_participante VARCHAR2(30)); 

CREATE SEQUENCE boleto_secuencia 
INCREMENT BY 1 
START WITH 1 
MAXVALUE 50 
NOCACHE 
NOCYCLE;

INSERT INTO tabla_concurso (boleto_id, nombre_participante, apellido_participante) 
VALUES (boleto_secuencia.NEXTVAL, 'Participante1', 'Perez'); 

INSERT INTO tabla_concurso (boleto_id, nombre_participante, apellido_participante) 
VALUES (boleto_secuencia.NEXTVAL, 'Participante 2', 'Ramirez');

INSERT INTO tabla_concurso (boleto_id, nombre_participante, apellido_participante) 
VALUES (boleto_secuencia.NEXTVAL, 'Participante 3', 'Cruz');
*/

17) Explica la siguiente línea de SQL: 

CREATE SYNONYM respaldo FOR copy_employees;

/*
R= Crea un sinonimo para la tabla copy_employees, de esta forma si queremos
hacer un query sobre esa tabla, ya no tendriamos que nombrarla como copy_employees
podria ser ahora: SELECT * FROM respaldo
*/

18) Define una tabla con el nombre "estudiantes", con los siguientes campos y sus características: 

		1.* "estudiante_id" -> con longitud de 5 y que no sea NULL 
		2.* "estudiante_nombre" -> con longitud de 20 y que no sea NULL 
		3.* "estudiante_apellido_paterno" -> con longitud de 20 y que permita valores NULL 
		4.* "estudiante_apellido_materno" -> con longitud de 20 y que permita valores NULL 
		5.* "estudiante_numero" -> con longitud de 15 que permita valores NULL 

Y que contenga un CONSTRAINT con el nombre "uk_estudiantes" a nivel tabla en donde se 
indique que el "estudiante_apellido_paterno" y "estudiante_apellido_materno" 
tienen que ser únicos en toda la tabla, haciendo uso de la keyword UNIQUE y mostrar que efectivamente se creó que CONSTRAINT para la tabla. 

/*
R=
CREATE TABLE estudiantes 
(estudiante_id NUMBER(5) NOT NULL, 
estudiante_nombre VARCHAR2(20) NOT NULL, 
estudiante_apellido_paterno VARCHAR2(20), 
estudiante_apellido_materno VARCHAR2(20), 
estudiante_numero NUMBER(15),
CONSTRAINT uk_estudiantes UNIQUE (estudiante_apellido_paterno,estudiante_apellido_materno));

SELECT* FROM USER_CONSTRAINTS WHERE table_name ='ESTUDIANTES';
*/

19) Crea una tabla llamada "jefes" con un id, una fecha de contratación y de
finalización, el id de la escuela y el id regional de la escuela, agregando
 un CONSTRAINT a la fecha_contratacion para que no sea NULL, crea un
 CONSTRAINT a nivel tabla donde se define una PRIMARY KEY compuesta por 
 dos campos y al final compara que la fecha de finalización sea mayor a 
 la de contratación 

/*
R=
CREATE TABLE jefes 
(jefes_id NUMBER(6,0), 
fecha_contratacion DATE CONSTRAINT constraint_contratacion NOT NULL, 
fecha_finalizacion DATE, 
escuela_id NUMBER(2), 
escuela_regional_id NUMBER(2,0), 
CONSTRAINT jefes_id PRIMARY KEY(jefes_id, fecha_contratacion), 
CONSTRAINT fechas_check CHECK (fecha_finalizacion > fecha_contratacion));
*/

20) Consulta el apellido, conviértelo a mayúsculas y reemplaza las A 
mayúsculas  -, concaténalos con el nombre en minúsculas, usa la tabla 
de employees. 
/*
R= SELECT ( REPLACE(UPPER(apellido), 'A', '-')) || '' || LOWER(nombre) 
*/

21) ¿Qué hace la siguiente consulta? 

	SELECT 	department_id, 
			manager_id, 
			AVG(salary) 
	FROM employees 
	GROUP BY ROLLUP (department_id, manager_id); 

/*
R= Mostrará el departmentid, el managerid, y el promedio(AVG) de salary de la
tabla empleados , y los agrupara por la combinacion de departmentid y managerid,
*/

22) Se desea generar una vista que contenga los siguientes datos: 
-first_name, last_name, employee_id y salary y que sea con los 
registros que tengan un salario entre $17,000 y $3,200 
/*
R=
CREATE VIEW vista_salarios 
AS SELECT employee_id, first_name, last_name, salary 
FROM employees 
WHERE salary <17000 and salary > 3200;
*/

23) ¿Qué datos se obtienen de hacer la siguiente unión de tablas? 

	SELECT  employees.last_name, 
			departments.department_name 
	FROM employees, departments; 
/*
R= Un producto cartesiano de ambas tablas, es decir, todas las posibles 
combinaciones de todas las filas de employees, con todas las filas de 
departments ya que no se especifico una clausa condicional where
*/

24) Considerando una lista de empleados y una lista de departamentos.
¿De qué forma es posible visualizar si cada empleado está asignado 
o no a un departamento?.
/*
R=
SELECT 	emp.last_name, 
		emp.department_id, 
		dep.department_name 
FROM employees emp, departments dep
WHERE emp.department_id = dep.department_id (+);

(+) eso indica que se efectuara una operacion left outer join, quiere decir
que se mostraran los registros de los empleados, aunque estos no tengan un 
departamento asignado
*/

25) ¿Qué hace la siguiente consulta? 

	SELECT LAST_DAY( (SELECT MAX(HIRE_DATE) 
	FROM EMPLOYEES 
	WHERE HIRE_DATE<SYSDATE)) 
	FROM DUAL; 
/*
R= Ubica cual es la fecha maxima de contratacion de la tabla empleados (la 
mas reciente), una vez con esa fecha, calcula el ultimo dia del mes de los empleados
cuya fecha de contratacion sea menor ala hora del sistema.
*/

26) ¿Qué hace la siguiente consulta? 

	SELECT HIRE_DATE, EMPLOYEE_ID, JOB_ID, TO_DATE(NULL) 
	FROM EMPLOYEES 
	WHERE EMPLOYEE_ID>(SELECT MIN(EMPLOYEE_ID) FROM employees) 
	UNION SELECT START_DATE,EMPLOYEE_ID,JOB_ID, END_DATE 
	FROM JOB_HISTORY 
	WHERE JOB_ID < (SELECT MAX(JOB_ID) FROM JOB_HISTORY) 
	ORDER BY EMPLOYEE_ID; 
/*
R= Mostrara los campos hire_Date, employeeid, jobid, y una fecha, de los registros
que coincidan con la condicion: valor minimo del empleadoID y jobid menor al valor maximo
de jobid
*/

27) Dadas las tablas Job_history y jobs, encuentre los Job_id 
que se intersecan. 
/*
R=
SELECT JOB_ID
FROM JOB_HISTORY INTERSECT SELECT JOB_ID
FROM JOBS
*/

28) Si quiero saber los nombres de empleados que fueron contratados 
después de Peter Vargas y no tengo la información de la fecha de 
contratación de Peter, ¿Cómo realizaría esa consulta?
/*
R=
*/

29) ¿Qué realiza la siguiente consulta? 

	SELECT last_name, salary 
	FROM employees 
	WHERE salary < (SELECT AVG(salary) FROM employees); 
/*
R= Semuestran los campos lastname y salary, de la tabla empleados
que cumplan con que su salario sea menor al promedio salarial redondeado de la misma tabla
*/

30) ¿Qué devuelve la consulta? 
}
	SELECT last_name, hire_date 
	FROM employees 
	WHERE EXTRACT(YEAR FROM hire_date) IN 
		(SELECT EXTRACT(YEAR FROM hire_date) 
		FROM employees 
		WHERE department_id=90); 
/*
R= Se muestran los campos lastname y hiredate de los empleados que esten en la condicion
que su año de contratacion este en el rango de valores de los años de contracion de los 
trabajadores del departamento 90.
*/

31) ¿Qué hace la siguiente consulta? 

	SELECT 	last_name, 
			salary, 
			NVL2(commission_pct, salary + (salary * commission_pct), salary) "Ganancia Total" 
	FROM employees 
	WHERE department_id IN(80,90); 
/*
R= Muestra los campos lastname, salary, y el calculo de salario x comision con el alias
ganancia total, si comision es null, evalua salary*comision, que tambien sera null,
por tanto si un empleado no tiene comision, la columna ganancia total, solo
reregresa su salaria,
todo esto de los trabajadores que esten unicamente en los deptos 80 o 90
*/

32) De la tabla employees, realiza un query que asigne un nombre a un 
departamento, debe cumplir con lo siguiente: 

Si el campo department_id = 90 asignale 'Gerencia' 
si el campo department_id = 80 asignale 'Ventas' 
si el campo department_id = 60 asignale 'Caja'
si no pertenece a ninguno asiganale la palabra OTRO 
/*
R=
SELECT last_name, CASE department_id
WHEN 90 THEN 'Gerencia'
WHEN 80 THEN 'Ventas'
WHEN 60 THEN 'Caja'
ELSE 'OTRO.'
END AS "Clasificacion"
FROM employees;
*/

33) ¿Qué hace la siguiente consulta? 

	SELECT department_id, job_id, count(*) AS conteo 
	FROM employees 
	WHERE department_id > 10 
	GROUP BY department_id, job_id 
	ORDER BY conteo; 
/*
R=  Mostrara los campos departmentid, jobid, y contara los registros por departmentid
y job ig, al final los ordenara por conteo que es el alias delcount
*/


34) Desarrolla el código que agrupe por departamentos de la tabla empleado 
y obtenga el salario máximo de cada uno de los grupos, a su vez cuente 
las veces que se repite cada uno de los departamentos, y únicamente 
muestre aquellos datos en el que el conteo fue mayor a 2 empleados 
por departamento. Finalmente, los datos sean ordenados por el salario 
máximo de forma ascendente. 
/*
R=
SELECT department_id, MAX(salary), count(*) AS num_departamentos
FROM employees
GROUP BY department_id HAVING count(*) > 2
ORDER BY MAX(salary);
*/

35) ¿Qué hará esta sentencia y bajo qué condiciones? 

INSERT ALL 
WHEN department_id IN (50) THEN INTO all_50s VALUES (employee_id, last_name, salary, department_id) 
WHEN department_id IN (90) THEN INTO all_90s VALUES (employee_id, last_name, salary, department_id) 
WHEN department_id IN (60) THEN INTO all_60s VALUES (employee_id, last_name, salary, department_id) 
SELECT employee_id, last_name, salary, department_id FROM employees; 
/*
R= evalua la condicion departmentid, y con base al resultado los inserta
a sus respectivas tablas, all50s, all60s, all90s segun sea el caso.
*/

36) ¿Qué hace la siguiente sentencia merge? 

	MERGE INTO backup_employees be USING employees e 
	ON (be.employee_id = e.employee_id) 
	WHEN MATCHED THEN UPDATE 
	SET be.salary = e.salary * 1.5 
	WHERE e.salary < (SELECT ROUND(AVG(salary)) 
	FROM employees 
	WHERE department_id = e.department_id) 
	WHEN NOT MATCHED THEN INSERT 
	VALUES (e.employee_id, e.first_name, e.last_name, e.email, 
		e.phone_number, e.hire_date, e.job_id, e.salary, 
		e.commission_pct, e.manager_id, e.department_id); 
/*
R=hara un backup de la tabla employees identificada como "e", en la tabla
backup_employees itentidifcada como "be", cuando los employeeid de ambas sean iguales
actualizara la columna salary de la tabla backupemployye, aumentando un 15% al
salario, si el salario es menor al promedio salarial redondeado, todo esto es si 
ya existen registros donde el emplloyeeid sea igual, si no existe un registro
de employees en backupemployees, el registro 
lo insertara como nuevo con los campos employyeid, firstname,lastname,emial.phonenumber, hiredate,
jobid,salary,comissionpct,managerid y dapartmentid
*/
