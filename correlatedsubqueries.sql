SELECT o.first_name, o.last_name, o.salary
FROM employees o
WHERE o.salary >
    (SELECT AVG(i.salary)
    FROM employees i
    WHERE i.department_id =
    o.department_id);
    
SELECT last_name AS "Not a Manager"
FROM employees emp
WHERE NOT EXISTS
    (SELECT *
    FROM employees mgr
    WHERE mgr.manager_id = emp.employee_id);
    
WITH managers AS
    (SELECT DISTINCT manager_id
    FROM employees
    WHERE manager_id IS NOT NULL)
SELECT last_name AS "Not a manager"
FROM employees
WHERE employee_id NOT IN
    (SELECT *
    FROM managers);