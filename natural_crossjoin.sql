/*Natural JOin
job_title esta en la tabla jobs, ahi se hace el join, el resultado es una vista*/
SELECT first_name, last_name, job_id, job_title
FROM employees NATURAL JOIN jobs
WHERE department_id > 80;

/*sin el natural join dandole un alias a las tablas*/
SELECT e.first_name, e.last_name, e.job_id, j.job_title
FROM employees e, jobs j
WHERE e.job_id = j.job_id 
AND e.department_id > 80;

/*Cross join
Une cada fila de una tabla con cada fila de otra tabla
el resultado puede ser muy largo
una tabla de 8 filas en cross join con otra tabla de 80
el resultado seria de 160 filas
PRODUCTO CARTESIANO, TODOS X TODOS*/
SELECT last_name, department_name
FROM employees CROSS JOIN departments;