/*Fecha mas baja de despido de algun trabajador*/
SELECT MIN(hire_date)
FROM employees;

/*Fecha mas reciente  o mayor de despido de algun trabajador*/
SELECT MAX(hire_date)
FROM employees;

/*Suma de los salarios*/
SELECT SUM(salary)
FROM employees
WHERE department_id= 90;

/*Promedio de los salrios*/
SELECT ROUND(AVG(salary), 2)
FROM employees
WHERE department_id= 90;

/*Salario maximo y minimo  de los empleados del departamento con id 60*/
SELECT MAX(salary), MIN(salary), MIN(employee_id)
FROM employees
WHERE department_id = 60;