/*Fecha del servidor menos la fecha de contratacion, entre 7,
para saber cuantas semanas lleva contratado el empleado*/
SELECT last_name, (SYSDATE - hire_date)/7 FROM employees;

/*Fecha en que dejo de trabajar, menos la fecha de inicio, alias "ternure job",
dividido en 365 para saber el tiempo trabajado en a�os*/
SELECT employee_id, (end_date-start_date)/365 AS "Tenure in last job" FROM job_history;

/*MONTHS_BETWEEN regresa un entero, no un tipo date*/

/*Saber el numero de meses entre la fecha del servidor y la fecha de despido,
y mostrar solo aquellos empleados que hayan trabajado menos de 240 meses*/
SELECT last_name, hire_date FROM employees WHERE MONTHS_BETWEEN(SYSDATE, hire_date) < 240;

/*Saber el numero de meses entre la fecha del servidor y la fecha de despido,
y mostrar solo aquellos empleados que hayan trabajado mas de 240 meses*/
SELECT last_name, hire_date FROM employees WHERE MONTHS_BETWEEN(SYSDATE, hire_date) > 240;

/*SUMAR 12 meses a la fecha del servidor*/
SELECT ADD_MONTHS (SYSDATE, 12) AS "Next Year" FROM dual;

/*Conocer la fecha del siguiente s�bado, DEBE DE SER EN ESPA�OL Y CON ACENTO*/
SELECT NEXT_DAY (SYSDATE, 's�bado') AS "Next Saturday" FROM dual;

/*Conocer el ultimo dia del mes, alias "end of the month"*/
SELECT LAST_DAY (SYSDATE) AS "End of the Month" FROM dual;

/*Redondea al siguiente mes la fecha de despido*/
SELECT hire_date, ROUND(hire_date, 'Month') FROM employees WHERE department_id= 50;

/*Mostrar employeeid y hire date*/
SELECT employee_id, hire_date,
/*Redodnear los meses entre la fecha del servidor  y la fecha de despido,
con el alias "TERNURE"*/
ROUND(MONTHS_BETWEEN(SYSDATE, hire_date)) AS TENURE,
/*Agrefa 6 meses a la fecha de despido, alias "review"*/
ADD_MONTHS (hire_date, 6) AS REVIEW,
/*fecha del viernes siguiente a la fecha de despido, y ultimo dia del mes de despido*/
NEXT_DAY(hire_date, 'Viernes'), LAST_DAY(hire_date)
FROM employees
/*Solo de los empleados con mas de 36 meses de duracion*/
WHERE MONTHS_BETWEEN (SYSDATE, hire_date) > 36;



