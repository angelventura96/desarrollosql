/*Creas una "tabla virtual" para almacenar la logica de un query y poder acceder a esa informacion
de manera rapida mediante un select*/
CREATE VIEW view_employees
AS SELECT employee_id,first_name, last_name, email
FROM employees
WHERE employee_id BETWEEN 100 and 124;

/*Consulta a la infromacion del query almacenado en la vista view_employees*/
select * from view_employees;

CREATE VIEW view_dept50
AS SELECT department_id, employee_id,first_name, last_name, salary
FROM copy_employees
WHERE department_id= 50;

select * from view_dept50;

UPDATE view_dept50
SET department_id= 90
WHERE employee_id= 124;

CREATE OR REPLACE VIEW view_dept50
AS SELECT department_id, employee_id, first_name, last_name, salary
FROM employees
WHERE department_id = 50
WITH READ ONLY;

/*Subquery dentro de una clausula from*/
SELECT e.last_name, e.salary, e.department_id, d.maxsal
FROM employees e,
    (SELECT department_id, max(salary) maxsal /*El segundo select sera la segunda tabla de*/
    FROM employees                            /*la clausula from y se identificara como "d"*/
    GROUP BY department_id) d
WHERE e.department_id = d.department_id
AND e.salary = d.maxsal;
order by e.department_id

SELECT ROWNUM AS "Longest employed", last_name, hire_date
FROM employees
WHERE ROWNUM <=5
ORDER BY hire_date;
