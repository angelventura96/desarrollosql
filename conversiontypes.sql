/*Convertir la fecha de despido, de tipo date, a varchar en formato
mes, dia y a�os*/
SELECT TO_CHAR(hire_date, 'Month dd, YYYY')
FROM employees;

SELECT TO_CHAR(hire_date, 'Month ddth, YYYY')
FROM employees;

/*darle formato de dinero*/
SELECT salary, TO_CHAR(salary,
'$99,999.00') AS "Salary"
FROM employees;

SELECT TO_DATE('May10,1989', 'fxMonDD,YYYY') AS "Convert"
FROM DUAL;