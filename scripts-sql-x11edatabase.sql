
SELECT last_name AS name, commission_pct AS comm FROM hr.employees;

SELECT last_name "Name", salary*12 "Annual Salary" FROM hr.employees;

DESC hr.departments;

SELECT department_id || department_name "prueba" FROM hr.departments;

SELECT last_name|| ' has a monthly salary of ' || salary || ' dollars.' AS Pay
FROM hr.employees;

SELECT last_name ||' has a '|| 1 ||' year salary of '|| salary*12 || ' dollars.' AS Pay FROM hr.employees;

SELECT department_id FROM hr.employees;

SELECT last_name, department_id FROM hr.employees;

SELECT DISTINCT last_name, department_id FROM hr.employees;

SELECT last_name, department_id FROM hr.employees where hire_date > '01-01-2000';

SELECT * from hr.employees where commission_pct is not null;

SELECT * from hr.employees where commission_pct is null;

SELECT last_name, salary FROM hr.employees WHERE salary BETWEEN 9000 AND 11000;

SELECT city, state_province, country_id FROM hr.locations WHERE country_id IN('UK', 'CA');

SELECT last_name FROM hr.employees WHERE last_name LIKE '_o%';

SELECT last_name FROM hr.employees WHERE last_name LIKE '%on';

SELECT last_name, job_id FROM hr.EMPLOYEES WHERE job_id LIKE '%\_R%' ESCAPE '\';

SELECT last_name||' '||salary*1.05 As "Employee Raise"
FROM hr.employees
WHERE department_id IN(50,80) AND first_name LIKE 'C%'
OR last_name LIKE '%s%';