/*Evaluar con case, si es x pon x, pero si es y, pon y, pero si es z pon z*/
SELECT last_name as "apellido", department_id as "departamento",
CASE department_id /*evalua el department id*/
WHEN 90 THEN 'Management'
WHEN 80 THEN 'Sales'
WHEN 60 THEN 'It'
ELSE 'Other dept.'
END AS "Department"
FROM employees;

/*IF ELIF ELSE*/
SELECT last_name,
DECODE(department_id,
90, 'Management',
80, 'Sales',
60, 'It',
'Other dept.')
AS "Department"
FROM employees;