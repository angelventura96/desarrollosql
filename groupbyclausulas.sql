/*Muestra por departamento el promedio salarial de ese departamento*/
SELECT department_id, AVG(salary)
FROM employees
GROUP BY department_id
ORDER BY department_id;

/*Mostrar el salario maximo de cada departamento ordenado por departamento*/
SELECT department_id, MAX(salary)
FROM employees
GROUP BY department_id
ORDER BY department_id;

/*Cauntos empleados hay por cada departamento*/
SELECT COUNT(last_name), department_id
FROM employees
GROUP BY department_id
ORDER BY department_id;

/*Agrupar por departemntoid y despues subgrupos por cada jobid y saber 
cuantos empleados hay por cada combinacion*/
SELECT department_id, job_id, count(*)
FROM employees
WHERE department_id > 40
GROUP BY department_id, job_id
ORDER BY department_id, job_id;

/*Calcula el promedio de salario por cada departamento 
y regresa el valor MAX,por eso solo muestra un resultado*/
SELECT max(avg(salary))
FROM employees
GROUP by department_id;

/*Se quiere conocer el salario mas alto por departamento, pero solo
de los departamentos que tengan mas de un empleado
para eso usamos la clausula HAVING, y la condicion ser�a COUNT(*)>1
cuenta todos, y debe ser mayuor a 1*/
SELECT department_id,MAX(salary)
FROM employees
GROUP BY department_id
HAVING COUNT(*)>1
ORDER BY department_id;


