UPDATE copy_employees
SET phone_number = '123456'
WHERE employee_id = 303;

select * from copy_employees where employee_id = 303;

UPDATE copy_employees
SET salary = (SELECT salary
FROM copy_employees
WHERE employee_id = 100)
WHERE employee_id = 101;

select * from copy_employees where employee_id = 303;

UPDATE copy_employees
SET salary = (SELECT salary
FROM copy_employees
WHERE employee_id = 205),
job_id = (SELECT job_id
FROM copy_employees
WHERE employee_id = 205)
WHERE employee_id = 206;

select * from copy_employees where employee_id = 206;

ALTER TABLE copy_employees
ADD (department_name varchar2(30));

UPDATE copy_employees e
SET e.department_name = (SELECT d.department_name
FROM departments d
WHERE e.department_id = d.department_id);




