/*nombre, apellido y fecha de contracion de la tabla empleado
donde la fecha de contratacion sea mayor a la fecha de contracion del
select aninado (fecha de contratacion de vargas que es 09/07/06)*/
SELECT first_name, last_name, hire_date
FROM employees
WHERE hire_date > (SELECT hire_date /*Fecha de contratacion de Vargas*/
                    FROM employees
                    WHERE last_name = 'Vargas'); /*09/07/06*/
                    
SELECT last_name
FROM employees
WHERE department_id =
                    (SELECT department_id /*marca error por que regreesa mas de una fila de resultado*/
                    FROM employees
                    WHERE last_name = 'Grant');
                    
/*Conocer quienes trabajan en marketiong pero no sabemos el id de marketing*/                    
SELECT last_name, job_id, department_id
FROM employees
WHERE department_id =
                    (SELECT department_id /*Conocer el id de marketing*/
                    FROM departments
                    WHERE department_name = 'Marketing')
ORDER BY job_id;

/*Conocer los empelados que ganan menos que el promedio salarial de todos los empelados*/
SELECT last_name, salary
FROM employees
WHERE salary <
                (SELECT AVG(salary)
                FROM employees);
                
SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) > /*2100 es el salario minimo de la tabla empleados*/
                    (SELECT MIN(salary)
                    FROM employees
                    WHERE department_id= 50)
order by department_id;