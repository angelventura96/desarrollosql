CREATE TABLE copy_employees
AS (select * from employees);

CREATE TABLE copy_departments
AS (SELECT * FROM departments);

describe copy_employees

INSERT INTO copy_departments
(department_id, department_name, manager_id, location_id)
VALUES
(200,'Human Resources', 205, 1500);

select * from copy_departments where department_id = 200;

INSERT INTO copy_departments
VALUES
(210,'Estate Management', 102, 1700);

select * from copy_departments where department_id = 210;

INSERT INTO copy_employees
(employee_id, first_name, last_name, phone_number, hire_date,
job_id, salary)
VALUES
(302,'Grigorz','Polanski', '8586667641', '15-Jun-2017',
'IT_PROG',4200);

INSERT INTO copy_employees
(employee_id, first_name, last_name, email, phone_number,
hire_date, job_id, salary)
VALUES
(302,'Grigorz','Polanski', 'gpolanski', '', '15-Jun-2017',
'IT_PROG',4200);

INSERT INTO copy_employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(304,'Test',USER, 't_user', 4159982010, SYSDATE, 'ST_CLERK',2500);

select * from copy_employees where employee_id=304;

INSERT INTO copy_employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(301,'Katie','Hernandez', 'khernandez','8586667641',
TO_DATE('Julio 8, 2017', 'Month fmdd, yyyy'), 'MK_REP',4200);

select * from copy_employees where employee_id = 301;

INSERT INTO copy_employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(303,'Angelina','Wright', 'awright','4159982010',
TO_DATE('Julio 10, 2017 17:20', 'Month fmdd, yyyy HH24:MI'),
'MK_REP', 3600);

select * from copy_employees where employee_id = 303;

SELECT first_name, last_name,
TO_CHAR(hire_date, 'dd-Mon-YYYY HH24:MI') As "Date and Time"
FROM copy_employees
WHERE employee_id= 303;

INSERT INTO sales_reps(id, name, salary, commission_pct)
SELECT employee_id, last_name, salary, commission_pct
FROM employees
WHERE job_id LIKE '%REP%';



