SELECT last_name, hire_date
FROM employees
ORDER BY hire_date; -------ORDEN ASCENDENTE

SELECT last_name, hire_date
FROM employees
ORDER BY hire_date DESC;  -----ORDEN DESCENDENTE

SELECT last_name, hire_date AS "Date Started"
FROM employees
ORDER BY "Date Started";-------ORDEN POR ALIAS ASCENDENTE

SELECT department_id, last_name
FROM employees
WHERE department_id <= 50
ORDER BY department_id, last_name;----ORDEN POR DEPTO, Y LUEGO POR APELLIDO

SELECT department_id, last_name
FROM employees
WHERE department_id <= 50
ORDER BY department_id DESC, last_name;---ORDEN POR DEPTO DESCENDIENTE Y LUEGO APELLIDO ASC

SELECT department_id, last_name
FROM employees
WHERE department_id <= 50
ORDER BY department_id DESC, last_name DESC;----ORDEN POR DEPTO Y LUEGO APELLIDO AMBOS DESC


FUNCIONES

Select last_name from employees where LOWER(last_name)='abel';