CREATE SEQUENCE runner_id_seq
INCREMENT BY 1
START WITH 1
MAXVALUE 50000
NOCACHE
NOCYCLE;

SELECT sequence_name, min_value, max_value, increment_by, last_number
FROM user_sequences;

INSERT INTO departments
(department_id, department_name, location_id)
VALUES (departments_seq.NEXTVAL, 'Support', 2500);

select * from departments 

