SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN /*si regresara mas de 1 opcion el query*/
                                    (SELECT EXTRACT(YEAR FROM hire_date)
                                    FROM employees
                                    WHERE department_id=90);

/*Con any si hay muchos valores tomara cualqiera
si la condicion es menor que any, tomara el datomayor
si la condicion es mayor que any, tomara el dato menor
si la condicion es igual a any, tomara los valores que hagan match con todos los valores de any*/     
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) < ANY
        (SELECT EXTRACT(YEAR FROM hire_date)
        FROM employees
        WHERE department_id=90);

SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) < ANY
    (SELECT salary
    FROM employees
    WHERE department_id IN (10,20))
ORDER BY department_id;

SELECT employee_id, manager_id, department_id
FROM employees
WHERE(manager_id,department_id) IN /*100,80 y 149,174*/
    (SELECT manager_id,department_id
    FROM employees
    WHERE employee_id IN (149,174))
AND employee_id NOT IN (149,174) /*que no este entre 149 y 174*/

/*empleado , manaher y depto que el manaher este entre 100 y 149
y que el departamento sea igual a 80*/
SELECT employee_id, manager_id, department_id
FROM employees
WHERE manager_id IN /*100 , 149*/
    (SELECT manager_id
    FROM employees
    WHERE employee_id IN
    (149,174))
AND department_id IN /*80*/
    (SELECT department_id
    FROM employees
    WHERE employee_id IN
    (149,174))
AND employee_id NOT IN(149,174); /*excluir el empleadoid 149 y 174*/

